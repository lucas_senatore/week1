var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function(tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl)
    })
    /* script para los cambios en los tooltips */
var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'))
var popoverList = popoverTriggerList.map(function(popoverTriggerEl) {
        return new bootstrap.Popover(popoverTriggerEl)
    })
    /* script para los cambios en los popovers */
var myCarousel = document.querySelector('[data-bs-ride="carousel"]');
var carousel = new bootstrap.Carousel(myCarousel, {
    interval: 2000
});
/* script para los cambios en el carousel */

var contacto = document.getElementById('contacto')
    //tomar id del modal
contacto.addEventListener('show.bs.modal', function(event) {
    console.log('El modal se esta mostrando');
})

var contacto = document.getElementById('contacto')
    //tomar id del modal
contacto.addEventListener('shown.bs.modal', function(event) {
    console.log('El modal se mostró');

    $('* #contactoBtn').removeClass('btn-outline-secondary');
    $('* #contactoBtn').addClass('btn-info');
    $('* #contactoBtn').prop('disabled', true);


})

var contacto = document.getElementById('contacto')
    //tomar id del modal
contacto.addEventListener('hide.bs.modal', function(event) {
    console.log('El modal se esta ocultando');
})

var contacto = document.getElementById('contacto')
    //tomar id del modal
contacto.addEventListener('hidden.bs.modal', function(event) {
    console.log('El modal se ocultó');
    $('* #contactoBtn').removeClass('btn-info');
    $('* #contactoBtn').addClass('btn-outline-secondary');
    $('* #contactoBtn').prop('disabled', false);
})